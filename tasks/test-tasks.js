const gulp          = require('gulp');
const _             = require('lodash');
const mocha         = require('gulp-mocha');
const istanbul      = require('gulp-istanbul');
const argv          = require('yargs').argv;
const remapIstanbul = require('remap-istanbul');
const fs            = require('fs');
const assert        = require('assert');
const packageJson   = require('../package.json');
const del           = require('del');


const coverageDir = './coverage/';
/**
 * Code instrumentation for istanbul
 * @param  {String} rootDir the root directory where the transpiled code was written
 * @return {[type]}         [description]
 */
function preTest (rootDir) {
  return () => gulp.src(`${rootDir}/lib/**/*.js`)
    .pipe(istanbul())
    .pipe(istanbul.hookRequire());
}

function getDefaultReporter () {
  return (process.env.NODE_ENV === 'development' ? 'mocha-pretty-bunyan-nyan' : 'spec');
}

function runTests (rootDir, testType, requiredModules = []) {
  // TODO: find a better way to do this and avoid using the module name in package.json...
  // (the module req-from used by gulp-mocha complicates things a bit)
  requiredModules.push(`${process.cwd()}/node_modules/${packageJson.name}/utils/test-common`);

  const mochaOpts = {
    require: requiredModules,
    reporter: argv.reporter || getDefaultReporter(),
  };

  if (argv.reporterOutput) {
    _.set(mochaOpts, 'reporterOptions.output', argv.reporterOutput);
  }

  return () => {
    assert(fs.existsSync(`${rootDir}/test`), `Test directory '${rootDir}/test' doesn't exist!`);

    let dirGlob;
    switch (testType) {
      case 'unit':
        // run only unit tests
        dirGlob = [`${rootDir}/test/**/*.spec.js`, `!${rootDir}/test/integration/**/*.spec.js`, `!${rootDir}/test/functional/**/*.spec.js`];
        break;
      case 'integration':
        // include unit tests in order to get the full coverage report but exclude functional tests
        dirGlob = [`${rootDir}/test/**/*.spec.js`, `!${rootDir}/test/functional/**/*.spec.js`];
        break;
      case 'functional':
        // run only functional tests
        dirGlob = `${rootDir}/test/functional/**/*.spec.js`;
        break;
      default:
        // run all tests
        dirGlob = `${rootDir}/test/**/*.spec.js`;
    }

    const stream = gulp.src(dirGlob)
      .pipe(mocha(mochaOpts))
      .once('error', (error) => {
        console.error(error.message);
        process.exit(1);
      });

    if (testType !== 'functional') {
      // we only need the json report for the `remapIstanbul` module
      stream.pipe(istanbul.writeReports({
        dir: coverageDir,
        reporters: ['json'],
      }))
      // .pipe(istanbul.enforceThresholds({
      //   thresholds: { global: 90 },
      // }))
      .on('end', () => {
        remapIstanbul(`${coverageDir}/coverage-final.json`, {
          html: coverageDir,
          text: null,
          // cobertura: coverageDir,
          lcovonly: `${coverageDir}/lcov.info`,
        })
        // Force exit: gulp-moha & istanbul remap are buggy
        .then(() => process.exit(0))
        .catch(() => process.exit(0));
      });
    } else {
      stream.on('end', () => process.exit(0));
    }

    return stream;
  };
}

function runUnitTests (rootDir, requiredModules = []) {
  return runTests(rootDir, 'unit', requiredModules);
}

function runIntegrationTests (rootDir, requiredModules = []) {
  return runTests(rootDir, 'integration', requiredModules);
}

function runFunctionalTests (rootDir, requiredModules = []) {
  return runTests(rootDir, 'functional', requiredModules);
}

function clean () {
  return del([`${process.cwd()}/reports`, `${process.cwd()}/coverage`]);
}

module.exports = {
  clean,
  preTest,
  runUnitTests,
  runIntegrationTests,
  runFunctionalTests,
};
